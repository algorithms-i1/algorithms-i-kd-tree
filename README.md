# Algorithms I Assignment 5: Kd-tree
Solution for assignment 5 of the Princeton University Algorithms I course on Coursera (https://coursera.cs.princeton.edu/algs4/assignments/kdtree/specification.php).

The goal of this project was to implement efficient range search and nearest neighbour search for a set of points on a 2d plane.
PointSET.java showcases a brute force solution using a pre-made red-black BST from the Algs4 library as its underlying data structure.
KdTree.java is an optimized implementation of a 2d tree which leverages pruning rules to speed up the range and neighbour searches.

## Results  

Correctness:  33/35 tests passed

Memory:       16/16 tests passed

Timing:       41/42 tests passed

<b>Aggregate score:</b> 96.10%


## How to use
Both classes are data structures that can be imported and used as is.
<b>NOTE:</b> Depends on the algs4 library used in the course.

/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

import java.util.ArrayList;

public class KdTree {
    private static final boolean X_ORDER = true;
    private static final boolean Y_ORDER = false;
    private Node root;
    private int size;

    private static class Node {
        private Point2D key;
        private Node left, right;
        private RectHV rectangle;
        private int depth;

        public Node(Point2D key, int depth, RectHV rectangle) {
            this.key = key;
            this.depth = depth;
            this.rectangle = rectangle;
        }

        public boolean sortsByX() {
            return this.depth % 2 == 0 ? Y_ORDER : X_ORDER;
        }
    }

    public KdTree() {
        size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public boolean contains(Point2D key) {
        if (key == null) throw new IllegalArgumentException();
        Node x = root;
        while (x != null) {
            if (key.equals(x.key)) return true;
            int cmp = compareNodes(key, x);
            if (cmp < 0) x = x.left;
            else if (cmp > 0) x = x.right;
        }
        return false;
    }

    public void insert(Point2D key) {
        if (key == null) throw new IllegalArgumentException();
        if (contains(key)) return;
        root = insert(root, null, key);
        size++;
    }

    private Node insert(Node x, Node prev, Point2D key) {
        if (x == null) {
            RectHV rect = calculateRectangle(key, prev);
            int depth = prev == null ? 1 : prev.depth + 1;
            return new Node(key, depth, rect);
        }

        int cmp = compareNodes(key, x);
        if (cmp < 0) x.left = insert(x.left, x, key);
        else if (cmp > 0) x.right = insert(x.right, x, key);
        return x;
    }

    private RectHV calculateRectangle(Point2D key, Node parent) {
        if (parent == null) {
            return new RectHV(0, 0, 1, 1);
        }
        int cmp = compareNodes(key, parent);
        return parent.sortsByX() ? rectangleByX(parent, cmp) : rectangleByY(parent, cmp);
    }

    private RectHV rectangleByX(Node parent, int cmp) {
        return cmp < 0 ? new RectHV(parent.rectangle.xmin(), parent.rectangle.ymin(),
                                    parent.key.x(), parent.rectangle.ymax()) :
               new RectHV(parent.key.x(), parent.rectangle.ymin(),
                          parent.rectangle.xmax(), parent.rectangle.ymax());
    }

    private RectHV rectangleByY(Node parent, int cmp) {
        return cmp < 0 ? new RectHV(parent.rectangle.xmin(), parent.rectangle.ymin(),
                                    parent.rectangle.xmax(), parent.key.y()) :
               new RectHV(parent.rectangle.xmin(), parent.key.y(),
                          parent.rectangle.xmax(), parent.rectangle.ymax());
    }

    private int compareNodes(Point2D key, Node node) {
        return node.sortsByX() ? compareByX(key, node) : compareByY(key, node);
    }

    private int compareByX(Point2D key, Node node) {
        int cmp = Point2D.X_ORDER.compare(key, node.key);
        return cmp != 0 ? cmp : Point2D.Y_ORDER.compare(key, node.key);
    }

    private int compareByY(Point2D key, Node node) {
        int cmp = Point2D.Y_ORDER.compare(key, node.key);
        return cmp != 0 ? cmp : Point2D.X_ORDER.compare(key, node.key);
    }

    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) throw new IllegalArgumentException();
        if (isEmpty()) return null;
        ArrayList<Point2D> found = new ArrayList<>();
        range(root, rect, found);
        return found;
    }

    private void range(Node curr, RectHV rect, ArrayList<Point2D> found) {
        if (rect.contains(curr.key)) {
            found.add(curr.key);
        }
        if (curr.left != null && rect.intersects(curr.left.rectangle)) {
            range(curr.left, rect, found);
        }
        if (curr.right != null && rect.intersects(curr.right.rectangle)) {
            range(curr.right, rect, found);
        }
    }

    public Point2D nearest(Point2D key) {
        if (key == null) throw new IllegalArgumentException();
        return isEmpty() ? null : nearest(root, key, root);
    }

    private Point2D nearest(Node curr, Point2D key, Node champ) {
        if (curr == null) return champ.key;
        if (key.equals(curr.key)) return curr.key;
        double champDist = champ.key.distanceSquaredTo(key);
        if (curr.key.distanceSquaredTo(key) < champDist) {
            champ = curr;
        }
        if (curr.left == null || curr.left.rectangle.distanceSquaredTo(key)
                > champDist) {
            return nearest(curr.right, key, champ);
        }
        if (curr.right == null || curr.right.rectangle.distanceSquaredTo(key)
                > champDist) {
            return nearest(curr.left, key, champ);
        }
        return nearestBothSubtrees(curr, key, champ);
    }

    private Point2D nearestBothSubtrees(Node curr, Point2D key, Node champ) {
        Point2D champLeft = null;
        Point2D champRight = null;
        double lRectDist = curr.left.rectangle.distanceSquaredTo(key);
        double rRectDist = curr.right.rectangle.distanceSquaredTo(key);
        if (lRectDist < rRectDist) {
            champLeft = nearest(curr.left, key, champ);
            if (champLeft.distanceSquaredTo(key)
                    > rRectDist) {
                champRight = nearest(curr.right, key, champ);
            }
        }
        else {
            champRight = nearest(curr.right, key, champ);
            if (champRight.distanceSquaredTo(key)
                    > lRectDist) {
                champLeft = nearest(curr.left, key, champ);
            }
        }
        return closestChamp(champLeft, champRight, key);
    }

    private Point2D closestChamp(Point2D champLeft, Point2D champRight, Point2D key) {
        if (champLeft == null) return champRight;
        if (champRight == null) return champLeft;
        if (champLeft.distanceSquaredTo(key)
                < champRight.distanceSquaredTo(key)) return champLeft;
        return champRight;
    }

    public void draw() {
        draw(root, new RectHV(0, 0, 1, 1));
    }

    private void draw(Node x, RectHV rect) {
        if (x == null) return;

        StdDraw.setPenRadius(0.01);
        StdDraw.setPenColor(StdDraw.BLACK);
        x.key.draw();
        StdDraw.setPenRadius();

        if (x.sortsByX()) {
            StdDraw.setPenColor(StdDraw.RED);
            StdDraw.line(x.key.x(), rect.ymin(), x.key.x(), rect.ymax());
            draw(x.left, new RectHV(rect.xmin(), rect.ymin(), x.key.x(), rect.ymax()));
            draw(x.right, new RectHV(x.key.x(), rect.ymin(), rect.xmax(), rect.ymax()));
        }
        else {
            StdDraw.setPenColor(StdDraw.BLUE);
            StdDraw.line(rect.xmin(), x.key.y(), rect.xmax(), x.key.y());
            draw(x.left, new RectHV(rect.xmin(), rect.ymin(), rect.xmax(), x.key.y()));
            draw(x.right, new RectHV(rect.xmin(), x.key.y(), rect.xmax(), rect.ymax()));
        }
    }
}